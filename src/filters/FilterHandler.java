/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filters;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java_1_02.Settings;

/**
 *
 * @author Ashvin
 */
public class FilterHandler {

    private Settings settings;
    private String header;
    private StringBuilder seq;
    

    /**
     * Constructor which start performing filters per protein sequence and
     * description.
     *
     * @param settings
     */
    public FilterHandler(Settings settings) {
        this.settings = settings;
       
    }

    /**
     * Preforms filter if certain option is given and a returns 
     * a arraylist with results.
     *
     * @param header
     * @param seq
     * @return ArrayList<String> results per protein of filter
     */
    public ArrayList<String> preformFilters(String header, StringBuilder seq) {
        this.header = header;
        this.seq = seq;
        ArrayList<String> allResults = new ArrayList<>();
       
        if (settings.getRegex() != null) {allResults.add(findSequenceRegex());}
        if (settings.getPrositePatern() != null) {allResults.add(findSequenceProsite());}
        if (settings.getOrganism() != null) {allResults.add(findSequenceOrganismName());}
        if (settings.getId() != null) {allResults.add(findSequenceId());}
        if (settings.getDescription() != null) {allResults.add(findSequenceDescription());}
        return allResults;
    }

    /**
     * Find which sequence contains the given regex pattern.
     * 
     * @return String result
     */
    public String findSequenceRegex() {
        Pattern pattern = Pattern.compile(this.settings.getRegex());
        Matcher m = pattern.matcher(this.seq);
        String result = null;
        if (m.find()) {
            result = "The regex = " + this.settings.getRegex() + " was found in the sequence = " + this.seq;
        }
        return result;
    }

    /**
     * Transform a prosite pattern to a regex pattern.
     *
     * @param prositePattern
     * @return string prosite pattern transformed to regex pattern
     */
    public String makeRegexPatern(String prositePattern) {

        prositePattern = prositePattern.replace(" ", "");
        prositePattern = prositePattern.replace("-", "");
        prositePattern = prositePattern.replace("X", ".");
        prositePattern = prositePattern.replace("x", ".");
        prositePattern = prositePattern.replace("{", "[^");
        prositePattern = prositePattern.replace("}", "]");
        prositePattern = prositePattern.replace("(", "{");
        prositePattern = prositePattern.replace(")", "}");
        prositePattern = prositePattern.replace(">", "$");
        prositePattern = prositePattern.replace("<", "^");
        return prositePattern;

    }

    /**
     * Finds which sequence contains the given prosite pattern.
     *
     * @return String result
     */
    public String findSequenceProsite() {

        // First transforms the prosite pattern to a regex
        String result = null;
        Pattern pattern = Pattern.compile(makeRegexPatern(this.settings.getPrositePatern()));
        Matcher m = pattern.matcher(this.seq);
        if (m.find()) {
            result = "The sequence = " + this.seq + "\n was found with this prosite pattern = " + this.settings.getPrositePatern();
        }
        return result;
    }

    /**
     * Find the sequence which belongs to the given organism.
     *
     * @return String result
     */
    public String findSequenceOrganismName() {

        String result = null;
        Pattern pattern = Pattern.compile(this.settings.getOrganism());
        Matcher m = pattern.matcher(this.header);
        if (m.find()) {
            result = ">" + this.settings.getOrganism() + "\n" + this.seq;
        }
        return result;
    }

    /**
     * Find the sequence which belongs to the given id.
     *
     * @return String result
     */
    public String findSequenceId() {

        String result = null;
        Pattern pattern = Pattern.compile(this.settings.getId());
        Matcher m = pattern.matcher(this.header);
        if (m.find()) {
            result = ">" + this.settings.getId() + "\n" + this.seq;
        }
        return result;
    }

    /**
     * Finds the sequence which belongs to the given wildcard.
     *
     * @return String result
     */
    public String findSequenceDescription() {

        String result = null;
        Pattern pattern = Pattern.compile(this.settings.getDescription());
        Matcher m = pattern.matcher(this.header);
        if (m.find()) {
            result = ">" + this.settings.getDescription() + "\n" + this.seq;
        }
        return result;

    }

}
