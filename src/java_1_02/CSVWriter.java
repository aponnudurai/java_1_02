/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package java_1_02;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

/**
 *
 * @author Ashvin
 */
public class CSVWriter {
    private final String outputFile;

    /**
     * Constructor which set output file name.
     *
     * @param outputFile
     */
    public CSVWriter(String outputFile){
        this.outputFile = outputFile;
        makeCSVFile();

    }

    /**
     * Makes a CSV file which will be used to write the content of the protein
     * fasta file.
     *
     * @exception IOException
     */
    private void makeCSVFile() {
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter(this.outputFile);
            PrintWriter output = new PrintWriter(fileWriter);

            // write header to CSV file
            output.write("gi,ref,taxID,protein,organism,sequence\n");

            output.close();
            fileWriter.close();

        } catch (IOException ex) {
            System.out.println("IO exception: " + ex.getLocalizedMessage());
        } finally{try {
            fileWriter.close();
            } catch (IOException ex) {
                System.err.println("IOException: "+ ex.getLocalizedMessage());
            }
}

    }

    /**
     * Writes content of the protein fasta file to a CSV file
     *
     * @param header
     * @param seq
     * @exception IOException 
     * @exception ArrayIndexOutOfBoundsException
     */
    public void writeCSVFile(String header, StringBuilder seq){
        FileWriter fileWriter = null;
        try {

            fileWriter = new FileWriter(this.outputFile, true);
            PrintWriter output = new PrintWriter(fileWriter);

            // Separate all the content of the header of each protein
            String[] headerSplited = header.split("\\|");
     
            String gi = headerSplited[1];
            String ref = headerSplited[3];
            String tax = headerSplited[4].split("=")[1];
            String[] protein_organism = headerSplited[5].split("\\[");
            String protein = protein_organism[0].trim();
            String proteinReplaced = protein.replace("\t", " ");
            String organism = protein_organism[1].substring(0, protein_organism[1].length() - 1);
            String organismReplaced = organism.replace("\t", " ");

            // append each protein info to CSV file
            output.write(gi + "," + ref + "," + tax + "," + proteinReplaced + "," + organismReplaced + "," + seq + "\n");
            output.close();
        } catch(ArrayIndexOutOfBoundsException ex){
            System.err.println("The file contains invalid content");
            System.exit(1);
        } catch (IOException ex) {
            System.out.println("IO exception: " + ex.getLocalizedMessage());
        }  finally {
            try {
                fileWriter.close();
            } catch (IOException ex) {
                System.err.println("IOException: " + ex.getLocalizedMessage());
            }
        }
    }
}
