/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package java_1_02;


import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

/**
 *
 * @author Ashvin
 */
public class Cli {

    private Options options;
    private CommandLine cmd;
    private Settings settings;
    private String[] args;

    /**
     * Constructor
     */
    Cli(String[] args) throws Exception {

        this.args = args;
        this.options = defineOptions();

        CommandLineParser parser = new BasicParser();
        this.settings = new Settings();
        try {
            this.cmd = parser.parse(this.options, args);
        } catch (ParseException ex) {
            System.err.println(ex.getLocalizedMessage());
            System.exit(1);
        } 
        checkOptions();
        processOptions();

    }

    /**
     * Define all the options.
     * 
     * @return Options
     */
    public Options defineOptions() {

        Options opt = new Options();

        opt.addOption("h", false, "Help function");
        opt.addOption("input", true, "Give an input file");
        opt.addOption("output", true, "Give a output file");
        opt.addOption("findProsite", true, "Give prosite pattern");
        opt.addOption("findRegex", true, "Give regex pattern which which will be searched in the sequences");
        opt.addOption("findOrganism", true, "Give a organism name");
        opt.addOption("findId", true, "Give a only TAXID number");
        opt.addOption("findDescription", true, "Give a wildcard as a string");

        return opt;

    }

     /**
     * Checks the given options and arguments.
     *
     * @return Options
     */
    private void checkOptions() {

        if (this.args.length == 0) {
            usage();
        }

        if (this.cmd.hasOption("h")) {
            usage();
        }

        if (!(this.cmd.hasOption("input"))) {
            System.err.println("Please input a protein fasta file");
            System.exit(1);
        }
    }

    /**
     * Set all given options.
     */
    private void processOptions() {

        if (this.cmd.hasOption("input")) {
            this.settings.setInput(this.cmd.getOptionValue("input"));
        }
        if (this.cmd.hasOption("output")) {
            this.settings.setOutput(this.cmd.getOptionValue("output"));
        }
        if (this.cmd.hasOption("findProsite")) {
            this.settings.setPrositePatern(this.cmd.getOptionValue("findProsite"));
        }
        if (this.cmd.hasOption("findRegex")) {
            this.settings.setRegex(this.cmd.getOptionValue("findRegex"));
        }
        if (this.cmd.hasOption("findOrganism")) {
            this.settings.setOrganism(this.cmd.getOptionValue("findOrganism"));
        }
        if (this.cmd.hasOption("findId")) {
            this.settings.setId(this.cmd.getOptionValue("findId"));
        }
        if (this.cmd.hasOption("findDescription")) {
            this.settings.setDescription(this.cmd.getOptionValue("findDescription"));
        }

    }

    /**
     * Prints which options can be given and their description.
     */
    public void usage() {
        HelpFormatter helpFormatter = new HelpFormatter();
        helpFormatter.printHelp("For analysing protein fasta files please use one the folowing options ", options);
        System.exit(0);
    }

    public Settings getSettings() {
        return settings;
    }

}
