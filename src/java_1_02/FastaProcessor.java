/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package java_1_02;

import filters.FilterHandler;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
/**
 *
 * @author Ashvin
 */
public class FastaProcessor {

    private Settings settings;
    private FilterHandler filterHandler;
    private CSVWriter writer;
  
    /**
     * Constructor which starts processing.
     * 
     * @param settings
     */
    public FastaProcessor(Settings settings) {
        this.settings = settings;
        
        if (settings.getOutput() != null) {this.writer = new CSVWriter(settings.getOutput());}
        fastaReader();

    }
    /**
     * Starts reading given file and filters per protein.
     * 
     * @param settings
     */
    private void fastaReader() {

        FilterHandler filterHandler = new FilterHandler(this.settings);
        try {
            FileReader fr = new FileReader(this.settings.getInput());
            BufferedReader br = new BufferedReader(fr);

            String line = null;
            String header = "";
            StringBuilder seq = new StringBuilder();
            int counter = 0;

            while ((line = br.readLine()) != null) {

                if (line.startsWith(">")) {
                    if (!(header.equals(""))) {

                        counter++;
                        // write to CSV file per protein
                        if (settings.getOutput() != null) {this.writer.writeCSVFile(header, seq);}
                        
                        // filter per protein and show results
                        outputResults(filterHandler.preformFilters(header, seq), counter);
                        seq.delete(0, seq.length());
                    }
                    header = line;
                } else {seq.append(line);}
            }
            if (header.equals("")) {
                System.err.println("The file is empty or it is missing a header");
                System.exit(1);}
            
            // process last protein in the given file
            counter++;
            if (settings.getOutput() != null) {this.writer.writeCSVFile(header, seq);}
            outputResults(filterHandler.preformFilters(header, seq), counter);

        } catch (FileNotFoundException ex) {
            System.err.println("The given file is not found");
        } catch (IOException ex) {
            System.err.println("IOException: " + ex.getLocalizedMessage());
        }
    }
    
    /**
     * Shows the results per protein.
     * 
     * @param allResults
     * @param protein
     */
    public void outputResults(ArrayList<String> allResults, int protein) {
        System.out.println("Protein number = " + protein);
        if (allResults.isEmpty()) {
            System.out.println("There are no results found");
        } else {
            for (String result : allResults) {
                if (result == null) {System.out.println("There are no results found");} 
                else {System.out.println(result);}
            } 
        }
    }
}
